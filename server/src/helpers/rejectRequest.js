export const rejectRequest = (
  res,
  message = 'Ups! Something went wrong',
  code = 500
) => {
  return res.status(code).json({
    errors: {
      msg: message.toString()
    }
  });
};
