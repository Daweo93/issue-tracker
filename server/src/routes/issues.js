import express from 'express';
import IssueController from '../controllers/issues';
import issueValidations from './validation/issues';

const router = express.Router();

router.get('/', IssueController.getAll);
router.post('/', issueValidations.addNew, IssueController.addNew);
router.patch('/:id', issueValidations.update, IssueController.update);

export default router;
