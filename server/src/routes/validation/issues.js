import { check } from 'express-validator/check';

export default {
  addNew: [
    check('title')
      .not()
      .isEmpty()
      .withMessage('Title is required')
  ],
  update: [
    check('dateUpdated')
      .not()
      .isEmpty()
      .withMessage('Update date is required'),
    check('status')
      .not()
      .isEmpty()
      .withMessage('Status is required')
      .isIn(['Open', 'Pending', 'Closed'])
      .withMessage("Status value must be one of 'Open', 'Pending', 'Closed'")
  ]
};
