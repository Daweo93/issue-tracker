import bodyParser from 'body-parser';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';

import config from './config/config';
// Connect to database
import { catchErrors, notFound } from './middlewares/errors';
import issuesRoutes from './routes/issues';

mongoose.Promise = global.Promise;
mongoose.connect(config.database, { useNewUrlParser: true }).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use('/api/issues/', issuesRoutes);

// errors handling
app.use(notFound);
app.use(catchErrors);

// let's play!
app.listen(config.server.port, () => {
  console.log(`Server is up on http://localhost:${config.server.port}`);
});

export default app;
