export function notFound(req, res, next) {
  const err = new Error('404 page not found');
  err.status = 404;
  next(err);
}

export function catchErrors(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    error: {
      code: err.status,
      message: err.message
    }
  });
}
