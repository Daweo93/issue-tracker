export default {
  env: process.env.NODE_ENV,
  server: {
    port: Number(process.env.PORT)
  },
  database: 'mongodb://localhost:27017/test'
};
