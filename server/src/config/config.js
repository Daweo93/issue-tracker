// required environment variables
['NODE_ENV', 'PORT'].forEach(name => {
  if (!process.env[name]) {
    throw new Error(`Environment variable ${name} is missing`);
  }
});

switch (process.env.NODE_ENV) {
  case 'development':
    module.exports = require('./dev');
    break;
  case 'test':
    module.exports = require('./test');
    break;
}
