export default {
  env: process.env.NODE_ENV,
  server: {
    port: Number(process.env.PORT)
  },
  database: 'mongodb://issue-tracker-mongodb:27017/db'
};
