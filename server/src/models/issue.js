import mongoose, { Schema } from 'mongoose';

const issueSchema = new Schema({
  _id: Schema.Types.ObjectId,
  title: {
    type: String,
    require: true
  },
  description: {
    type: String,
    default: ''
  },
  status: {
    type: String,
    default: 'Open'
  },
  dateAdded: {
    type: Date,
    default: Date.now
  },
  dateUpdated: {
    type: Date,
    default: Date.now
  }
});

const Issue = mongoose.model('Issue', issueSchema);
export default Issue;
