import mongoose from 'mongoose';
import chai from 'chai';
import Issue from '../models/issue';
import app from '../index';

const should = chai.should();

chai.use(require('chai-http'));

describe('Issues', () => {
  beforeEach(done => {
    Issue.find().remove({}, err => {
      done();
    });
  });

  describe('/GET issues', () => {
    it('it should GET all the issues', done => {
      chai
        .request(app)
        .get('/api/issues')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('issues');
          res.body.issues.should.be.a('array');
          res.body.issues.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST Issue', () => {
    it('should not POST an issue without title', done => {
      chai
        .request(app)
        .post('/api/issues')
        .send({})
        .end((err, res) => {
          res.should.have.status(422);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('title');
          res.body.errors.title.should.have
            .property('msg')
            .eql('Title is required');
          done();
        });
    });

    it('should POST an issue', done => {
      const issue = {
        title: 'Complete recruitment app',
        description: 'Example description'
      };

      chai
        .request(app)
        .post('/api/issues')
        .send(issue)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('title').eql(issue.title);
          res.body.should.have.property('description').eql(issue.description);
          res.body.should.have.property('status').eql('Open');
          res.body.should.have.property('dateAdded');
          res.body.should.have.property('dateUpdated');
          done();
        });
    });
  });

  describe('/PATCH Issue', () => {
    it('should not PATCH an issue with incorrect ID', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/5b4a045a1d26b500efdb5139`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Open' })
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have
              .property('msg')
              .eql('Error: Issue not found');
            done();
          });
      });
    });

    it('should not PATCH an issue without updateDate', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description'
      });
      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have.property('dateUpdated');
            res.body.errors.dateUpdated.should.have
              .property('msg')
              .eql('Update date is required');
            done();
          });
      });
    });

    it('should not PATCH an issue without status', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString() })
          .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have.property('status');
            res.body.errors.status.should.have
              .property('msg')
              .eql('Status is required');
            done();
          });
      });
    });

    it('should not PATCH an issue with incorrect status', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Other' })
          .end((err, res) => {
            res.should.have.status(422);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have.property('status');
            res.body.errors.status.should.have
              .property('msg')
              .eql("Status value must be one of 'Open', 'Pending', 'Closed'");
            done();
          });
      });
    });

    it('should not PATCH an issue with the same status', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Open' })
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have
              .property('msg')
              .eql('Error: Status can not be set to the same type');
            done();
          });
      });
    });

    it('should not PATCH an issue status to previous stage PENDING => OPEN', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description',
        status: 'Pending'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Open' })
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have
              .property('msg')
              .eql('Error: Status can not be set to previous level: Open');
            done();
          });
      });
    });

    it('should not PATCH an issue status to previous stage CLOSED => PENDING', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description',
        status: 'Closed'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Pending' })
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have
              .property('msg')
              .eql(
                'Error: Status can not be set to previous level: Open or Pending'
              );
            done();
          });
      });
    });

    it('should not PATCH an issue status to previous stage CLOSED => OPEN', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description',
        status: 'Closed'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Open' })
          .end((err, res) => {
            res.should.have.status(500);
            res.body.should.be.a('object');
            res.body.should.have.property('errors');
            res.body.errors.should.have
              .property('msg')
              .eql(
                'Error: Status can not be set to previous level: Open or Pending'
              );
            done();
          });
      });
    });

    it('should PATCH an issue status OPEN => PENDING', done => {
      const issue = new Issue({
        _id: new mongoose.Types.ObjectId(),
        title: 'Complete recruitment app',
        description: 'Example description',
        status: 'Open'
      });

      issue.save((err, issue) => {
        chai
          .request(app)
          .patch(`/api/issues/${issue._id}`)
          .send({ dateUpdated: new Date().toISOString(), status: 'Pending' })
          .end((err, res) => {
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('issue');
            res.body.issue._id.should.be.eql(issue.id);
            res.body.issue.status.should.be.eql('Pending');
            done();
          });
      });
    });
  });
});
