import { rejectRequest } from '../helpers/rejectRequest';
import Issue from '../models/issue';
import { validationResult } from 'express-validator/check';
import { Types } from 'mongoose';

const IssueController = {};

IssueController.getAll = (req, res) => {
  Issue.find()
    .sort({ dateUpdated: -1 })
    .exec()
    .then(response => {
      res.status(200).json({ issues: response });
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

IssueController.addNew = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.mapped()
    });
  }

  const { title, description } = req.body;

  const issue = new Issue({
    _id: new Types.ObjectId(),
    title: title,
    description: description
  });

  issue
    .save()
    .then(response => {
      res.status(201).json(response);
    })
    .catch(error => {
      res.status(500).json({
        error
      });
    });
};

IssueController.update = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.mapped()
    });
  }

  const { status, dateUpdated } = req.body;

  Issue.findOne({ _id: req.params.id })
    .select('status dateUpdated')
    .exec()
    .then(doc => {
      if (doc) {
        if (doc.status === status) {
          throw new Error('Status can not be set to the same type');
        }

        if (doc.status === 'Pending' && status === 'Open') {
          throw new Error('Status can not be set to previous level: Open');
        }

        if (
          doc.status === 'Closed' &&
          ['Open', 'Pending'].indexOf(status) > -1
        ) {
          throw new Error(
            'Status can not be set to previous level: Open or Pending'
          );
        }

        return doc;
      } else {
        throw new Error('Issue not found');
      }
    })
    .then(doc => {
      Issue.findByIdAndUpdate(
        doc.id,
        { $set: { status, dateUpdated } },
        { new: true }
      )
        .exec()
        .then(response => {
          res.status(201).json({
            issue: response
          });
        })
        .catch(error => {
          rejectRequest(res, error);
        });
    })
    .catch(error => {
      rejectRequest(res, error);
    });
};

export default IssueController;
