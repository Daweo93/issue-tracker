// @flow
import { combineReducers } from 'redux';
import type { Actions, State, StateIsFetching } from '../types/issues';
import byId from './byId';

const handleErrors = errors => {
  return Object.keys(errors).map(error => errors[error].msg);
};

const all = (state: Object = {}, action: Actions): State => {
  switch (action.type) {
    case 'FETCH_ISSUES_SUCCESS':
      return { ...state, ...action.response.entities.issues };
    case 'UPDATE_ISSUE_SUCCESS':
      const id = action.response.result;
      const issue = action.response.entities.issues[id];
      return {
        ...state,
        [id]: { ...issue }
      };
    case 'ADD_ISSUE_SUCCESS':
      const newId = action.response.result;
      return {
        ...state,
        [newId]: action.response.entities.issues[newId]
      };
    default:
      return state;
  }
};

const isFetching = (state: StateIsFetching = false, action: Actions) => {
  switch (action.type) {
    case 'FETCH_ISSUES_REQUEST':
      return true;
    case 'FETCH_ISSUES_FAIL':
    case 'FETCH_ISSUES_SUCCESS':
      return false;
    default:
      return state;
  }
};

const error = (state: string = '', action: Actions) => {
  switch (action.type) {
    case 'ADD_ISSUE_FAIL':
    case 'UPDATE_ISSUE_FAIL':
    case 'FETCH_ISSUES_FAIL':
      return handleErrors(action.error);
    case 'FETCH_ISSUES_SUCCESS':
    case 'FETCH_ISSUES_REQUEST':
    case 'UPDATE_ISSUE_REQUEST':
    case 'UPDATE_ISSUE_SUCCESS':
      return null;
    default:
      return state;
  }
};

const issues = combineReducers({
  all,
  byId,
  isFetching,
  error
});

export default issues;
export const getIssue = (state: State, id: string) => state[id];
export const getAllIssues = (state: State) => {
  return state.issues.byId.map(id => getIssue(state.issues.all, id));
};
