// @flow
import type { Actions, StateById } from '../types/issues';

const byId = (state: StateById = [], action: Actions) => {
  switch (action.type) {
    case 'FETCH_ISSUES_SUCCESS':
      return Array.from(new Set([...state, ...action.response.result]));
    case 'ADD_ISSUE_SUCCESS':
      return [...state, action.response.result];
    default:
      return state;
  }
};

export default byId;
