import { normalize } from 'normalizr';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { fetchIssues } from '../../actions';
import * as schema from '../../actions/schema';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Async actions', () => {
  it('should FETCH_ISSUES_SUCCESS when fetching issues has been done', () => {
    const issue = {
      description: '',
      status: 'Open',
      _id: '5b4a04a846545001157b63ca',
      title: 'Done app for recruitment process',
      dateAdded: '2018-07-14T14:11:52.343Z',
      dateUpdated: '2018-07-14T14:11:52.343Z',
      __v: 0
    };

    fetch.mockResponseOnce(
      JSON.stringify({
        issues: [issue]
      })
    );

    const expectedActions = [
      { type: 'FETCH_ISSUES_REQUEST' },
      {
        type: 'FETCH_ISSUES_SUCCESS',
        response: normalize([issue], schema.arrayOfIssues)
      }
    ];

    const store = mockStore({ issues: { byId: [], isFetching: false } });

    return store.dispatch(fetchIssues()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
