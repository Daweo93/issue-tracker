// @flow
import { shallow } from 'enzyme';
import React from 'react';
import { IssueList } from '../../../components/IssuesList/List';

describe('IssueList Component', () => {
  let cmp;
  let props;

  const list = () => {
    if (!cmp) {
      cmp = shallow(<IssueList {...props} />);
    }

    return cmp;
  };

  beforeEach(() => {
    props = {
      isFetching: false,
      fetchIssues: jest.fn(),
      issues: [
        {
          description: 'Issue 1 description',
          status: 'Open',
          _id: '5b4a04e7a6f8d2015851ca77',
          title: 'Issue 1',
          dateAdded: '2018-07-14T14:12:55.282Z',
          dateUpdated: '2018-07-14T14:12:55.282Z'
        },
        {
          description: 'Issue 2 description',
          status: 'Open',
          _id: '5b4a1fb91693f1045755cc71',
          title: 'Issue 2 title',
          dateAdded: '2018-07-14T16:07:21.167Z',
          dateUpdated: '2018-07-14T16:07:21.167Z'
        }
      ]
    };
    cmp = undefined;
  });

  it('should render Loader', () => {
    let cmp = list();
    cmp.setProps({ isFetching: true });
    expect(cmp.find('Loader').exists()).toBe(true);
    expect(cmp.find('List').exists()).toBe(false);
    expect(cmp.find('Header').exists()).toBe(false);
  });

  it('should render Header with message', () => {
    let cmp = list();
    cmp.setProps({ issues: [] });
    expect(cmp.find('Loader').exists()).toBe(false);
    expect(cmp.find('List').exists()).toBe(false);
    expect(cmp.find('Header').exists()).toBe(true);
  });

  it('should have 2 ListItem elements', () => {
    let cmp = list();
    expect(cmp.find('ListItem').length).toEqual(2);
  });
});
