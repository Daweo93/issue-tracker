// @flow
import { mount } from 'enzyme';
import React from 'react';
import { ListItemActions } from '../../../components/IssuesList/ListItemActions';

describe('ListItem Component', () => {
  let cmp;
  let props;

  const listItemActions = () => {
    if (!cmp) {
      cmp = mount(<ListItemActions {...props} />);
    }

    return cmp;
  };

  beforeEach(() => {
    props = {
      id: '5b49e148978d8501c7d929fb',
      status: 'Open',
      updateIssue: jest.fn()
    };

    cmp = undefined;
  });

  it('should call function on dropdown item change', () => {
    let cmp = listItemActions();
    cmp
      .find('DropdownItem')
      .at(0)
      .simulate('click');
    expect(props.updateIssue).toHaveBeenCalledTimes(1);
    expect(props.updateIssue).toHaveBeenCalledWith(props.id, 'Pending');
  });

  it('should have two items', () => {
    let cmp = listItemActions();
    expect(cmp.find('DropdownItem').length).toBe(2);
    expect(
      cmp
        .find('DropdownItem')
        .at(0)
        .text()
    ).toEqual('Pending');
    expect(
      cmp
        .find('DropdownItem')
        .at(1)
        .text()
    ).toEqual('Closed');
  });

  it('should have one item', () => {
    let cmp = listItemActions();
    cmp.setProps({ status: 'Pending' });
    expect(cmp.find('DropdownItem').length).toBe(1);
    expect(cmp.find('DropdownItem').text()).toEqual('Closed');
  });

  it("shouldn't render Dropdown", () => {
    let cmp = listItemActions();
    cmp.setProps({ status: 'Closed' });
    expect(cmp.find('Dropdown').exists()).toBe(false);
  });
});
