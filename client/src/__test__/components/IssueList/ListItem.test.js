// @flow
import { shallow } from 'enzyme';
import React from 'react';
import { ListItem } from '../../../components/IssuesList/ListItem';

describe('ListItem Component', () => {
  let cmp;
  let props;

  const listItem = () => {
    if (!cmp) {
      cmp = shallow(<ListItem {...props} />);
    }

    return cmp;
  };

  beforeEach(() => {
    props = {
      issue: {
        title: 'Done app for recruitment process',
        description: 'Description',
        status: 'Open',
        _id: '5b4a04a846545001157b63ca',
        dateAdded: '2018-07-14T14:11:52.343Z',
        dateUpdated: '2018-07-14T14:11:52.343Z'
      }
    };

    cmp = undefined;
  });

  describe('List item display provided values', () => {
    it('should contain Label', () => {
      expect(
        listItem()
          .find('Label')
          .exists()
      ).toBe(true);
      expect(
        listItem()
          .find('Label')
          .shallow()
          .text()
      ).toEqual(props.issue.status);
    });

    it('should contain issue name', () => {
      expect(
        listItem()
          .find('.issue-title')
          .text()
      ).toEqual(props.issue.title);
    });

    it('shouldnt have description and title should be a `span`', () => {
      const cmp = listItem();
      cmp.setProps({
        issue: {
          title: 'Done app for recruitment process',
          description: '',
          status: 'Open',
          _id: '5b4a04a846545001157b63ca'
        }
      });

      expect(cmp.find('Transition').exists()).toBe(false);
      expect(cmp.find('.issue-title').type()).toEqual('span');
    });

    it('should have hidden description', () => {
      const cmp = listItem();
      expect(cmp.find('Transition').exists()).toBe(true);
      expect(cmp.find('Transition').prop('visible')).toBe(false);
    });

    it('should contain description', () => {
      const cmp = listItem();
      cmp.find('.issue-title').simulate('click');
      expect(cmp.find('.issue-title').type()).toEqual('a');
      expect(cmp.find('Transition').prop('visible')).toBe(true);
    });
  });
});
