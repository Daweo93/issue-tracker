// @flow
import type { Actions as IssuesActions, State as IssuesState } from './issues';

export type Action = IssuesActions;
export type Store = IssuesState;
export type GetState = () => Store;
export type Dispatch = (
  action: Action | ThunkAction | PromiseAction | Array<Action>
) => any;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
