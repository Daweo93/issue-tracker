// @flow
export type Issue = {|
  +_id: string,
  +title: string,
  +description: string,
  +status: string,
  +dateAdded: string,
  +dateUpdated: string
|};

export type FetchIssuesRequest = {
  type: 'FETCH_ISSUES_REQUEST'
};

export type FetchIssuesSuccess = {|
  type: 'FETCH_ISSUES_SUCCESS',
  response: Object
|};

export type FetchIssuesFail = {
  type: 'FETCH_ISSUES_FAIL',
  error: Object
};

export type UpdateIssueRequest = {
  type: 'UPDATE_ISSUE_REQUEST'
};

export type UpdateIssueSuccess = {|
  type: 'UPDATE_ISSUE_SUCCESS',
  response: Object
|};

export type UpdateIssueFail = {
  type: 'UPDATE_ISSUE_FAIL',
  error: Object
};

export type addIssueRequest = {
  type: 'ADD_ISSUE_REQUEST'
};

export type addIssueSuccess = {|
  type: 'ADD_ISSUE_SUCCESS',
  response: Object
|};

export type addIssueFail = {
  type: 'ADD_ISSUE_FAIL',
  error: Object
};

export type Actions =
  | FetchIssuesRequest
  | FetchIssuesSuccess
  | FetchIssuesFail
  | UpdateIssueRequest
  | UpdateIssueSuccess
  | UpdateIssueFail
  | addIssueRequest
  | addIssueSuccess
  | addIssueFail;

export type StateById = Array<string>;
export type StateIsFetching = boolean;

export type State = {
  +issues: {
    +all: Object,
    +byId: StateById,
    +isFetching: StateIsFetching,
    +errors: string
  }
};
