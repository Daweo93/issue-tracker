import { schema } from 'normalizr';
export const issue = new schema.Entity('issues', {}, { idAttribute: '_id' });
export const arrayOfIssues = [issue];
