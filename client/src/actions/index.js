// @flow
import { normalize } from 'normalizr';
import type { Dispatch } from '../types';
import type { Actions } from '../types/issues';
import config from '../config';
import * as schema from './schema';

export const fetchIssuesRequest = (): Actions => ({
  type: 'FETCH_ISSUES_REQUEST'
});

export const fetchIssuesSuccess = (response: Object): Actions => ({
  type: 'FETCH_ISSUES_SUCCESS',
  response: normalize(response.issues, schema.arrayOfIssues)
});

export const fetchIssuesFail = (error: Object) => ({
  type: 'FETCH_ISSUES_FAIL',
  error
});

export const fetchIssues = () => (dispatch: Dispatch) => {
  dispatch(fetchIssuesRequest());

  return fetch(`${config.apiUrl}/issues`)
    .then(res => res.json())
    .then(res => dispatch(fetchIssuesSuccess(res)))
    .catch(err => dispatch(fetchIssuesFail(err)));
};

export const updateIssueRequest = (): Actions => ({
  type: 'UPDATE_ISSUE_REQUEST'
});

export const updateIssueSuccess = (response: Object): Actions => ({
  type: 'UPDATE_ISSUE_SUCCESS',
  response: normalize(response.issue, schema.issue)
});

export const updateIssueFail = (error: Object) => ({
  type: 'UPDATE_ISSUE_FAIL',
  error
});

export const updateIssue = (id: string, status: string) => (
  dispatch: Dispatch
) => {
  dispatch(updateIssueRequest());
  return fetch(`${config.apiUrl}/issues/${id}`, {
    method: 'PATCH',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify({
      status,
      dateUpdated: new Date().toISOString()
    })
  })
    .then(res => res.json())
    .then(res => {
      if (res.errors) {
        return dispatch(updateIssueFail(res.errors));
      }
      return dispatch(updateIssueSuccess(res));
    })
    .catch(err => dispatch(updateIssueFail(err)));
};

export const addIssueRequest = (): Actions => ({
  type: 'ADD_ISSUE_REQUEST'
});

export const addIssueSuccess = (response: Object): Actions => ({
  type: 'ADD_ISSUE_SUCCESS',
  response: normalize(response.issue, schema.issue)
});

export const addIssueFail = (error: Object) => ({
  type: 'ADD_ISSUE_FAIL',
  error
});

export const addIssue = (title: string, description: string) => (
  dispatch: Dispatch
) => {
  dispatch(addIssueRequest());
  return fetch(`${config.apiUrl}/issues`, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: JSON.stringify({
      title,
      description
    })
  })
    .then(res => res.json())
    .then(res => {
      if (res.errors) {
        return dispatch(addIssueFail(res.errors));
      }
      return dispatch(addIssueSuccess(res));
    })
    .catch(err => dispatch(addIssueFail(err)));
};
