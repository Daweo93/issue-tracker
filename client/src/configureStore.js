import { createStore, applyMiddleware } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const configureStore = () => {
  const middlewares = [thunk];

  return createStore(
    rootReducer,
    devToolsEnhancer(),
    applyMiddleware(...middlewares)
  );
};

export default configureStore;
