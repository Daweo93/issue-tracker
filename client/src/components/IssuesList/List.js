// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Header, List, Loader } from 'semantic-ui-react';
import { getAllIssues } from '../../reducers/issues';
import type { Store } from '../../types';
import type { Issue } from '../../types/issues';
import ListItem from './ListItem';
import { fetchIssues } from '../../actions';

type Props = {
  isFetching: boolean,
  issues: Array<Issue>,
  fetchIssues: () => void
};

class IssueList extends Component<Props> {
  componentDidMount() {
    this.props.fetchIssues();
  }

  sort = (issues: Array<Issue>): Array<Issue> => {
    const status = {
      Pending: 0,
      Open: 1,
      Closed: 2
    };

    return issues.sort((a, b) => {
      const s = status[a.status] - status[b.status];
      if (s !== 0) {
        return s;
      }
      return new Date(b.dateUpdated) - new Date(a.dateUpdated);
    });
  };

  render() {
    const { isFetching, issues } = this.props;

    if (isFetching) {
      return (
        <Loader active inline="centered">
          Loading
        </Loader>
      );
    }

    if (!isFetching && issues.length < 1) {
      return (
        <Header as="h3" textAlign="center">
          No issue added yet.
        </Header>
      );
    }

    const sortedIssues = this.sort(issues);

    return (
      <List divided verticalAlign="middle" relaxed="very">
        {sortedIssues.map(issue => <ListItem issue={issue} key={issue._id} />)}
      </List>
    );
  }
}

const mapStateToProps = (state: Store) => ({
  issues: getAllIssues(state)
});

export { IssueList };
export default connect(mapStateToProps, { fetchIssues })(IssueList);
