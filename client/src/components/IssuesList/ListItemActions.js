// @flow
import React from 'react';
import { connect } from 'react-redux';
import { updateIssue } from '../../actions';

import { Dropdown } from 'semantic-ui-react';

type Props = {|
  id: string,
  status: string,
  updateIssue: (id: string, option: string) => any
|};

const ListItemActions = ({ id, status, updateIssue }: Props) => {
  let options = [];

  if (status === 'Open') {
    options.push('Pending', 'Closed');
  } else if (status === 'Pending') {
    options.push('Closed');
  }

  const handleStatusChange = (event, { children }) => {
    updateIssue(id, children);
  };

  const renderItems = () => {
    return options.map((option, index) => (
      <Dropdown.Item key={index} onClick={handleStatusChange}>
        {option}
      </Dropdown.Item>
    ));
  };

  if (options.length) {
    return (
      <Dropdown icon="setting" pointing="right">
        <Dropdown.Menu>{renderItems()}</Dropdown.Menu>
      </Dropdown>
    );
  } else {
    return null;
  }
};

export { ListItemActions };
export default connect(null, { updateIssue })(ListItemActions);
