// @flow
import React, { Component } from 'react';
import { Button, Label, List, Transition } from 'semantic-ui-react';
import type { Issue } from '../../types/issues';
import ListItemActions from './ListItemActions';

type Props = {|
  issue: Issue
|};

type State = {|
  visible: boolean
|};

class ListItem extends Component<Props, State> {
  state = { visible: false };
  statusColor = {
    Open: 'green',
    Pending: 'orange',
    Closed: 'red'
  };

  toggleVisibility = () => this.setState({ visible: !this.state.visible });
  renderTitle = ({ title, description }: Issue) => {
    if (description.length) {
      return (
        <a onClick={this.toggleVisibility} className="issue-title">
          {title}
        </a>
      );
    }
    return <span className="issue-title">{title}</span>;
  };

  render() {
    const { issue } = this.props;
    const { visible } = this.state;

    return (
      <List.Item>
        <List.Content floated="right">
          <ListItemActions id={issue._id} status={issue.status} />
        </List.Content>
        <List.Content>
          <Label color={this.statusColor[issue.status]} horizontal>
            {issue.status}
          </Label>

          {this.renderTitle(issue)}

          {issue.description.length && (
            <Transition visible={visible} animation="scale" duration={500}>
              <p className="issue-description">{issue.description}</p>
            </Transition>
          )}
        </List.Content>
      </List.Item>
    );
  }
}

export { ListItem };
export default ListItem;
