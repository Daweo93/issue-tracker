import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

const AppHeader = () => (
  <Menu inverted>
    <Menu.Item header>
      <Link to="/">Issue tracker</Link>
    </Menu.Item>
    <Menu.Item position="right">
      <Link to="/add">Add issue</Link>
    </Menu.Item>
  </Menu>
);

export default AppHeader;
