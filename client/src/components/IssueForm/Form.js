// @flow
import { Formik } from 'formik';
import React from 'react';
import { connect } from 'react-redux';
import { Button, Form as UIForm, Header } from 'semantic-ui-react';
import { addIssue } from '../../actions';

type Props = {
  addIssue: (title: string, description: string) => any,
  history: () => any
};

const Form = ({ addIssue, history }: Props) => (
  <Formik
    initialValues={{
      title: '',
      description: ''
    }}
    validate={values => {
      let errors = {};
      if (!values.title) {
        errors.title = 'Title of issue is required';
      }
      return errors;
    }}
    onSubmit={values => {
      addIssue(values.title, values.description).then(() => {
        history.push('/');
      });
    }}
    render={({
      values,
      errors,
      touched,
      handleChange,
      handleBlur,
      handleSubmit,
      isSubmitting
    }) => (
      <UIForm onSubmit={handleSubmit}>
        <Header as="h1">Add new issue</Header>
        <UIForm.Field error={touched.title && !!errors.title}>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            name="title"
            id="title"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.title}
          />
        </UIForm.Field>
        <UIForm.Field>
          <label htmlFor="description">Description</label>
          <textarea
            id="description"
            name="description"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.password}
          />
        </UIForm.Field>
        <Button type="submit" disabled={isSubmitting} primary>
          Submit
        </Button>
      </UIForm>
    )}
  />
);

export { Form };
export default connect(null, { addIssue })(Form);
