import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { Grid } from 'semantic-ui-react';
import AppHeader from './AppHeader';
import Form from './IssueForm/Form';
import List from './IssuesList/List';

class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <AppHeader />
          <Grid container centered>
            <Grid.Column small={12} column={6}>
              <Route exact path="/" component={List} />
              <Route exact path="/add" component={Form} />
            </Grid.Column>
          </Grid>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
